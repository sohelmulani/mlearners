# you add libraries here too 
# make sure that the methods you write here have required libraries

import pandas as pd
import numpy as np
from sklearn import tree


class mlmethods(object):
	"""
	This class contains various machine learning algorithms
	as methods to predict class over iris dataset
	
	"""
	# this is like a constructor
	def __init__(self):
		pass




	#############################################################
	######## Specific function for X algorithm ##################
	#############################################################
	def algorithm(self):
		pass
		
	

	#############################################################
	#### Specific function for Decision tree classifier #########
	#############################################################
	
	
	def decision_tree(self, class_weight=None, criterion='gini', max_depth=None,
			max_features=None, max_leaf_nodes=None, random_state=47,
			splitter='best'):
	
		"""
		Decision tree classifier
		Documentation(https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html)
		args:
			criterion : string, optional (default=”gini”)
				The function to measure the quality of a split. 
				Supported criteria are “gini” for the Gini impurity and “entropy” for the information gain.

			max_depth : int or None, optional (default=None)
				The maximum depth of the tree.
			
			max_features : int, float, string or None, optional (default=None)
				The number of features to consider when looking for the best split:

				If int, then consider max_features features at each split.
				If float, then max_features is a fraction and int(max_features * n_features) features are considered at each split.

				If “auto”, then max_features=sqrt(n_features).
				If “sqrt”, then max_features=sqrt(n_features).
				If “log2”, then max_features=log2(n_features).
				If None, then max_features=n_features.
			
			max_leaf_nodes : max number of leaf nodes in tree
			
			splitter: The strategy used to choose the split at each node. 
				Supported strategies are “best” to choose the best split and 
				“random” to choose the best random split.


			Returns:
				decision tree classifier
			
		"""
		
		treeClf = tree.DecisionTreeClassifier(class_weight=class_weight, 
			criterion=criterion, max_depth=max_depth,max_features=max_features, max_leaf_nodes=max_leaf_nodes,
			random_state=random_state,splitter=splitter)

		
		print(treeClf)
		return treeClf



	# this is like a de-constructor
	def __del__(self):
		pass
		