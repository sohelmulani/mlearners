# you add libraries here too 
# make sure that the methods you write here have required libraries

import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split


class PreProcess(object):
	"""
	This class contains various methods to preprocess the data
	before feeding to algorithms defined in methods.py
	
	"""
	# this is like a constructor
	def __init__(self):
		pass



	#############################################################
	######## Specific function for  ###############
	#############################################################





	#############################################################
	######## Specific function for Min Max Scaler ###############
	#############################################################
	def min_max_scaler(self,dataX):
		'''
		Min Max Scaler

		args:
			dataX: Dataframe, array like structure of float/int values only

		returns:
			pp_dataX: min max scaled dataX
		'''
		scaler = MinMaxScaler()
		pp_dataX = scaler.fit_transform(dataX)
		return pp_dataX

	#############################################################
	######## Specific function for Normalization ################
	#############################################################

	def normalization(self,dataX):
		'''
		normalization

		args:
			dataX: Dataframe, array like structure of float/int values only

		returns:
			normalized dataX
		'''


		# normalize X
		pass


	#############################################################
	######## Specific function for Scaling ######################
	#############################################################


	def standardize(self,dataX):
		# use sklearn.preprocessing.scale()
		pass





	#############################################################
	##### Specific function for One Hot encoding ################
	#############################################################


	def one_hot_encoders(self,dataY):
		'''
		Convert categorical Y to one hot encodings

		args:
			dataY: labels (Dataframe)

		returns:
			one hot encoded labels (Dataframe)
		'''
		# convert categorical values to one hot encoders

		one_hot_encoders = pd.get_dummies(dataY)
		return one_hot_encoders
	

	#############################################################
	##### Specific function for test train split ################
	#############################################################

	def train_test_spit(self,dataX,dataY,random_state=47,test_size=0.2):
		'''
		Train test split method

		args:
			dataX: array like values
			dataY: labels array like values

		returns:
			trainX, testX, trainY, testY (tuple)
		'''

		trainX, testX, trainY, testY = train_test_split(dataX,dataY,
			random_state=random_state, test_size=test_size)

		print("Train size:",trainX.shape[0],"\nTest size: ",testY.shape[0])

		return (trainX, testX, trainY, testY)

	# this is like a de-constructor
	def __del__(self):
		pass
		